English glossary
================

First
-----

   available - доступный

   popular - популярный

   intelligent - умный

   successful - успешный

   knowledge - знание

   helpful - полезный

   wonderful - замечательный

   practical - практический

   cultural - культурный

   additional - дополнительный

   entertain - развлекать

   wander - бродить, странствовать

   elaborate - разрабатывать (прорабатывать)

   amuse - забавлять

   enjoy - наслаждаться

   worry - беспокоиться

   laugh - смех

   return - вернуть

   stop - остановить

   start - начать


Second
------

   minute - минута

   evident - очевидно

   straight - прямой

   inspire - вдохновлять

   device - устройство

   axe - топор

   allow - позволять

   brother-in-law - шурин

   budget - бюджет

   armpit - подмышка

   confusion - путаница

   courage - мужество

   cloudy - облачный

   appear - появляться

   physical - физическая

   sophisticated - сложный

   accommodation - проживание

   communication - общение

   anniversary - годовщина

   awful - ужасный

   neighbour - сосед