Linux LPIC
==========

Содержание:

.. toctree::
   :maxdepth: 2

   lpic101_1
   lpic101_2
   lpic101_3

   lpic102_1
   lpic102_2
   lpic102_3
   lpic102_4
   lpic102_5

   lpic103_1
   lpic103_2
   lpic103_3
   lpic103_4
   lpic103_5
   lpic103_6
   lpic103_7
   lpic103_8

   lpic104_1
   lpic104_2
   lpic104_3
   lpic104_4
   lpic104_5
   lpic104_6
   lpic104_7


Полезные ссылки:

      * `IBM LPIC materials <http://www.ibm.com/developerworks/ru/library/l-lpic1-v3-map/>`_

      * `Kirill Semaev LPIC materials videos <http://www.it-semaev.ru/courses.html>`_
