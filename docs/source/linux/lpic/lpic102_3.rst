LPIC 102-3 Manage shared libraries
==================================

Содержание:

   /lib — libs for apps from /bin;

   /usr/lib — libs for apps from /usr/bin;

   /etc/ld.so.conf — libs conf file;

   ``ldd`` — prints shared library dependencies (``ldd /bin/ls``);

   ``ldconfig`` — configures dynamic linker run-time bindings;

   LD_LIBRARY_PATH — path to libraries (``export LD_LIBRARY_PATH=/opt/app/lib``).
