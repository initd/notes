LPIC 103-4 Use streams, pipes and redirects
===========================================

Содержание:

   stdin — ``0`` standard input;

   stdout — ``1`` standard output;

   stderr — ``2`` standard error output;

   ``tee`` — read from standard input and write to standard output and files;

      ``ls | tee out_ls.txt`` — prints result of ls to stdout and writes result to output.txt;

   ``xargs`` — build and execute command lines from standard input;

      ``find . -name "*.txt" | xargs rm -rf`` - removes all *.txt files;

   ``>`` — redirect to write output;

   ``>>`` — redirect to append output;

   ``|`` — pipes output.
