LPIC 103-5 Create, monitor, and kill processes
==============================================

Содержание:

   ``cmd &`` — runs *cmd* job in background;

   ``bg [job_id]`` — moves the last (or job_id if provided) stopped job to background;

   ``fg [job_id]`` — moves background process to foreground;

   ``jobs`` — lists all active jobs;

   ``kill`` — sends a signal to a process;

   ``killall`` — kills processes by name;

   ``nohup`` —  runs a command immune to hangups, with output to a non-tty;

   ``ps`` — reports a snapshot of the current processes;

   ``pstree`` — displays a tree of processes;

   ``top`` — displays Linux processes;

   ``free`` — displays amount of free and used memory in the system;

   ``uptime`` —  tells how long the system has been running;

   ``pgrep`` — looks up processes based on name and other attributes;

   ``pkill`` — signal processes based on name and other attributes;

   ``screen`` — screen manager with VT100/ANSI terminal emulation;

   ``tmux`` — terminal multiplexer;

   ``sleep`` — delay for a specified amount of time;

