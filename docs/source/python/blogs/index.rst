Полезные блоги по Python
========================

   * `Lerner Consulting Blog <http://blog.lerner.co.il/>`_ with his `Python’s objects and classes — a visual guide <http://blog.lerner.co.il/pythons-objects-and-classes-a-visual-guide/>`_

   * `Python Tips book <http://book.pythontips.com/en/latest/index.html>`_

   * `Bruce Eckel Python 3 patterns, recipes and idioms <http://python-3-patterns-idioms-test.readthedocs.org/en/latest/>`_




